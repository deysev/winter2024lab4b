public class Snake{
	private String breed;
	private double length;
	private boolean venomous;
	
	public Snake(String breed, double length, boolean venomous){
		this.breed = breed;
		this.length = length;
		this.venomous = venomous;
	}
	
	public String getBreed() {
		return this.breed;
	}
	
	public void setLength(double length) {
		this.length = length;
	}
	public double getLength() {
		return this.length;
	}
	
	public boolean getVenomous() {
		return this.venomous;
	}
	
	public void beCautious() {
		if (venomous == true){
			System.out.print("Be cautious! The " + breed + " is venomous");
		} else {
			System.out.print("No need to worry " + breed + " is not venomous");
		}
	}

	public void snakeSize() {
		if (length >= 6.25){
			System.out.println(" and it is huge!");
		} else {
			System.out.println(" and it is quite small.");
		}
	}	
}