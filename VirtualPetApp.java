import java.util.Scanner;

public class VirtualPetApp {
    public static void main(String[] args) {
		java.util.Scanner reader = new java.util.Scanner(System.in);
        Snake[] den = new Snake[1];
        for (int i = 0; i < den.length; i++) {
			
            System.out.println("What breed is it?");
            String breed = reader.nextLine();
			
            System.out.println("How big is it? (in meters)");
            double length = reader.nextDouble();
			
            System.out.println("Is it venomous?(true or false)");
            boolean venomous = reader.nextBoolean();
			
			den[i] = new Snake(breed, length, venomous);
		}
		//use the setters and getters here
		//print("changing the value of the snake legnth")
		//USE THE SETTER
		//SHOW THE CHANGE IN A PRINT STATEMENT< USING THE GETTER
		System.out.println("Changing the value of snake length");
		System.out.println("Original length: " + den[0].getLength());
		den[0].setLength(2);
		System.out.println("New length: " + den[0].getLength());	
    }	
}
